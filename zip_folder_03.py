#
# This code: 
# 1. Scans stdout_01.txt
# 2. Extracts path and driectory names
# 3. Zips the directory
#
# test data path + folder (linux)
# full string: /var/www/html/samples/clangSingleFile/jg_reports/2018-09-27-100632-4965-1
#        path: /var/www/html/samples/clangSingleFile/jg_reports
#   directory: 2018-09-27-100632-4965-1

import re
import zipfile
import os
import sys

def zipfolder(foldername, target_dir):            
    zipobj = zipfile.ZipFile(foldername + '.zip', 'w', zipfile.ZIP_DEFLATED)
    rootlen = len(target_dir) + 1
    for base, dirs, files in os.walk(target_dir):
        for file in files:
            fn = os.path.join(base, file)
            zipobj.write(fn, fn[rootlen:])


stdout_file = open('stdout_01.txt','r')
for line in stdout_file:
	list = line.split(' ',2)
#	print list
#	print list[1]
	mystring = ''.join(list[1])
#	print mystring
	if 'Run' in mystring:
#		print "Report was generated"
		target_string = ''.join(list[2])
#		print target_string
		output = 1 # True (non-zero)
	else:
#		print "not target string"
		output = 0  # False
stdout_file.close()

if output:
	quoted_list = re.findall(r"'(.*?)'", target_string)
#	print quoted_list
	mystring2 = ''.join(quoted_list)
#	print mystring2
	mypath = mystring2.split(' ',1)[1]
#	print mystring2
#	print mypath
	myfolder_list = mypath.rsplit('/', 1)
#	print myfolder_list
	myfolder = myfolder_list[1]
	print myfolder
	zipfolder(myfolder, './jg_reports') #insert file and path names
	sys.exit()
#
